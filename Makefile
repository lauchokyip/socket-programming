#INSTRUCTIONS:
#1.Type "make" into the command line to compile your code
#2.Type "make test-short" to run the binary with "path_short.txt"
#3.Type "make test-very-long" to run the binary with "path_very_long.txt"
#NOTE : Make sure to always run "make" after editing your ".c" file
CC = gcc
HTTPCLIENT = httpclient
HTTPSERVER = udp_tcp_server
MULTISERVICE = multi_service_client
SERVER = localhost
HTTPPORT = 8888
UDPPORT = 8889
PATHNAME = test_short
SHIFT = 0

objects = $(HTTPCLIENT) $(HTTPSERVER)

all : $(objects)

$(objects): %: %.c
	$(CC) -g $< -o $@

test-server: $(HTTPSERVER)
	./$(HTTPSERVER) $(HTTPPORT) $(UDPPORT)

test-client: $(HTTPCLIENT)
	./$(HTTPCLIENT) $(SERVER) $(HTTPPORT) $(PATHNAME) $(SHIFT)
	
test-multi-service:
	./$(MULTISERVICE) $(SERVER) $(HTTPPORT) $(PATHNAME) $(UDPPORT) $(SHIFT) 

.PHONY : clean
clean:
	rm -f $(objects) httpreplies*
