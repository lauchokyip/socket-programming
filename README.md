# Simple HTTP 1.0 server in C

To test http server only,
```
make
make test-server
make test-client
```

To test http and udp server,
```
make
make test-server
make test-multi-service

```

### Result
![result](result.gif)