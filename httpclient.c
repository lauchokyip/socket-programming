#include <sys/socket.h>
#include <stdio.h>
#include <netdb.h>
#include <strings.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define MAX_BUFFER_SIZE 1024
#define MAX_LINE_SIZE 256




int open_clientfd(char *hostname, int port)
{
    int clientfd;
    struct hostent *hp;
    struct sockaddr_in serveraddr; 

    clientfd = socket(AF_INET, SOCK_STREAM, 0);
    if(clientfd < 0)
        return -1;
    
    hp = gethostbyname(hostname);
    if(hp == NULL)
        return -2;

    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)hp->h_addr,
          (char *)&serveraddr.sin_addr.s_addr, hp->h_length);
    serveraddr.sin_port = htons(port);

    if(connect(clientfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0)
        return -1;

    return clientfd;

}


int main(int argc, char **argv)
{

    int clientfd, port, ret;
    char *host, *path;
    char buff[MAX_BUFFER_SIZE], buff1[MAX_BUFFER_SIZE], first_line[MAX_LINE_SIZE], last_line[MAX_LINE_SIZE];

    if(argc != 5)
    {
        perror("Please insert the input as ./httpclient [servername] [port] [file path] [shift num]\n");
        exit(0);
    }

    host = argv[1];
    port = atoi(argv[2]);
    path = argv[3];
    
    clientfd = open_clientfd(host,port);

    if(clientfd < 0)
    {
        perror("Error opening connection \n");
        exit(0);
    }

    bzero((char *)buff,sizeof(buff));
    bzero((char *)buff1,sizeof(buff1));
    
    sprintf(buff,"GET %s %s HTTP/1.0\r\n\r\n",path, argv[4]);
    
    fputs(buff,stdout);
    

    if( write(clientfd, buff, strlen(buff)) < 0)
    {
        perror("Send failed");
        exit(0);
    }


    while( read(clientfd, buff1, MAX_BUFFER_SIZE) > 0)
    {
        fputs(buff1, stdout); 
        bzero(buff1,sizeof(buff1));
    } 
   
    close(clientfd); 
    exit(0); 

}
