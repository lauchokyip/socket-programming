#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <strings.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/select.h>


#define LISTENQ 10
#define MAXBUFF 1024



const char http200[] = "HTTP/1.0 200 OK\r\n\r\n";
const char http404[] = "HTTP/1.0 404 Not Found\r\n\r\n";
const char http403[] = "HTTP/1.0 403 Forbidden\r\n\r\n";

int my_max( int a, int b ) { return a > b ? a : b; }

int open_listen_tcp_fd(int port)  
{ 
  int listenfd, optval=1; 
  struct sockaddr_in serveraddr; 
   
  /* Create a socket descriptor */ 
  if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    return -1; 
  
  /* Eliminates "Address already in use" error from bind. */ 
  if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR,  
		 (const void *)&optval , sizeof(int)) < 0) 
    return -1; 
 
  /* Listenfd will be an endpoint for all requests to port 
     on any IP address for this host */ 
  bzero((char *) &serveraddr, sizeof(serveraddr)); 
  serveraddr.sin_family = AF_INET;  
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);  
  serveraddr.sin_port = htons((unsigned short)port);  
  
  //  bind function assigns a local protocol address to a socket
  if (bind(listenfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0) 
    return -1; 
 
  /* Make it a listening socket ready to accept 
     connection requests */ 
  if (listen(listenfd, LISTENQ) < 0) 
    return -1; 
 
  return listenfd; 
}

int open_listen_udp_fd(int port)  
{ 
  int listenfd, optval=1; 
  struct sockaddr_in serveraddr; 
   
  /* Create a socket descriptor */ 
  if ((listenfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) 
    return -1; 
  
  /* Eliminates "Address already in use" error from bind. */ 
  if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR,  
		 (const void *)&optval , sizeof(int)) < 0) 
    return -1; 
 
  /* Listenfd will be an endpoint for all requests to port 
     on any IP address for this host */ 
  bzero((char *) &serveraddr, sizeof(serveraddr)); 
  serveraddr.sin_family = AF_INET;  
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);  
  serveraddr.sin_port = htons((unsigned short)port);  
  
  //  bind function assigns a local protocol address to a socket
  if (bind(listenfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0) 
    return -1; 
  
  return listenfd; 
} 


int extract(char *buff, char *path, char *shift_num)
{
  
    int i = 0,j = 0,k = 0;
    char *temp;
    

    if(strncmp(buff,"GET",3) != 0 && strncmp(buff,"get",3) != 0)
    {
        perror("not a GET requests\n");
        return -1;
    }


    //add ./ to path
    path[0] = '.';
    path[1] = '/';

    //Search for token
    temp = strtok(buff, " ");

    //Split first token as path
    temp = strtok(NULL, " ");
    bcopy((char *) temp, (char *) (path + 2), strlen(temp) + 1 );

    //Spllit second token as shift num
    temp = strtok(NULL, " ");
    bcopy((char *) temp, (char *) shift_num, strlen(temp) + 1 );

    return 0;
    
}

int encrypt_buff(char * buff, int shift)
{
    int len,i;
    const int shift_max = 26;

    len = strlen(buff);

    for(i = 0 ; i < len; i++)
    {
        //if it's lower case letter
        if( buff[i] >= 'a' && buff[i] <= 'z' )
        {
            buff[i] = buff[i] - (shift % shift_max);
            if(buff[i] < 'a')
            {
                buff[i] = buff[i] + 'z' - 'a' + 1 ;
            }
        }

        //lower capital letter
        else if (buff[i] >= 'A' &&  buff[i] <= 'Z')
        {
            buff[i] = buff[i] - (shift % shift_max);
            if(buff[i] < 'A')
            {
                buff[i] = buff[i] + 'Z' - 'A' + 1 ;
            }
        }
        
        else
        {
            continue;
        }
        
    }

    
    return 0;
}


int print_out_response(int connfd, char *path, int shift)
{
    char buff[MAXBUFF];
    FILE *fptr = fopen(path,"r");

    if(fptr == NULL)
    {
        perror("file pointer should not be null\n");
        return -1;
    }


    bzero((char *) buff,sizeof(buff)); 
    while(fgets(buff, MAXBUFF, fptr) != NULL)
    {
        encrypt_buff(buff,shift);
        write(connfd,buff, strlen(buff) );
        bzero((char *) buff,sizeof(buff));
    }

    fclose(fptr);
    return 0;
}


void http_server_response(int connfd, char *path, int shift_num_int)
{
    FILE *fptr;

    char error[50];
    //reset errno
    errno = 0;

    //open the file
    fptr = fopen(path,"r");

    switch (errno)
    {
    case EACCES:
        write(connfd,http403, strlen(http403)); //sadjksjd/0 
        break;
    
    case ENOENT:
        write(connfd,http404,strlen(http404));
        break;

    case 0:
        write(connfd,http200,strlen(http200));
        print_out_response(connfd, path, shift_num_int);
        break;

    default:
        sprintf(error, "Errno number %d cannot be recognized", errno);
        write(connfd,error,sizeof(error));
    }

    if(fptr != NULL)
    {
        fclose(fptr);
    }
}

unsigned int process_ACK(char *buff_ACK)
{
    unsigned int ret;
    char each_byte;

    ret = 0;
  
    //get the first three bytes, shift 32, 16, 8 respectively
    for(int i = 1; i <= 4; i++)
    {
        ret |= ( ((unsigned char)buff_ACK[i-1]) << ( 8 * ( 4 - i ) ) );   
    }

    return ret;
}

char* resolve_name( char* buff_ip )
{
    struct in_addr ip; 
    struct hostent *hostp;
   
    if(!inet_aton(buff_ip, &ip))
    {
        perror("error while parsing the IP address\n");
        return NULL;
    }

    hostp = gethostbyaddr((struct in_addr *)&ip, sizeof(ip), AF_INET);
    if(hostp == NULL)
    {
        perror("error while resolving the hostname\n");
        return NULL;
    }

    return hostp->h_name;
}


char* concat_ACK_IP( char* hostname, unsigned int ACK)
{
    int i;
    unsigned int temp, total_length;
    char *out_client;

    if(hostname == NULL)
    {
        perror("hostname is NULL!\n");
        return NULL;
    }
    
    //total length
    total_length = strlen(hostname) + 4;

    //malloc to send back to client
    out_client = malloc(sizeof( *out_client) * total_length);

    if(out_client == NULL)
    {
        perror("malloc errors when concat ACK and IP!\n");
        return NULL;
    }

    //copy the hostname first
    bcopy((char *)hostname, (char *) out_client, strlen(hostname));

    //ACK +1
    ACK += 1;

    for(i = 1; i <= 4; i ++)
    {
        temp =  ACK & ( 0XFF << ( 8 * ( 4 - i ) ) );
        out_client[strlen(hostname) + (i-1) ] =  (temp >> 8 * ( 4 - i ));
    }

    return out_client;
}

int main(int argc, char **argv)
{
    int listen_tcp_fd, conn_tcp_fd, listen_udp_fd, conn_udp_fd; // file descriptor
    int http_port, udp_port;                                    // port for http and udp
    fd_set rfds;                                                // file descriptor set
    int client_tcp_len, client_udp_len;                         // client tcp/udp addr length
    struct sockaddr_in client_tcp_addr, client_udp_addr;        // client tcp/udp socket address

    // miscellaneous thing
    int shift_num_int, ret, childpid, maxfd, ip_len, i, j;      
    unsigned int ACK_int, total_length;
    char buff[MAXBUFF],buff_ip[128],buff_ACK[4], path[128], shift_num[128];
    char *hostname, *out_client ;

    if(argc != 3)
    {
        printf("Please insert the input as ./httpserver [http_port] [ping_port]\n");
        exit(0);
    }

    signal(SIGCHLD,SIG_IGN);

    http_port = atoi(argv[1]);
    udp_port = atoi(argv[2]);

    //return file descriptor binds by socker
    listen_tcp_fd = open_listen_tcp_fd(http_port);
    listen_udp_fd = open_listen_udp_fd(udp_port);

    if(listen_tcp_fd < 0 || listen_udp_fd < 0)
    {
        perror("Error when openning listen file descriptor\n");
    }

    while (1) 
    {

        //clear the socket set
        FD_ZERO(&rfds);
        
        // add file descriptor to the set
        FD_SET(listen_tcp_fd, &rfds);
        FD_SET(listen_udp_fd, &rfds);

        //set the max range to listen to
        maxfd = my_max(listen_tcp_fd, listen_udp_fd) + 1;

        // use select function
        if ( (ret = select(maxfd, &rfds, NULL, NULL, NULL)) == 0)
        {
            perror("No sockets are ready\n");
            exit(0);
        }

        //if tcp connection is set
        if(FD_ISSET(listen_tcp_fd, &rfds))
        {
                //accepting tcp connection from client
            client_tcp_len = sizeof(client_tcp_addr); 
            conn_tcp_fd= accept(listen_tcp_fd, (struct sockaddr *)&client_tcp_addr, &client_tcp_len);

            if(conn_tcp_fd < 0)
            {
                perror("Error when openning connection file descriptor\n");
                exit(0);
            }

            if( (childpid = fork()) == 0)
            {
                
                if( (ret = close(listen_tcp_fd)) < 0)
                {
                    perror("Close connection file descriptor failed\n");
                    exit(0);
                }
                
                //read GET from the user
                bzero((char *)buff, MAXBUFF);
                if ( (ret = read(conn_tcp_fd, buff, MAXBUFF)) < 0)
                {
                    perror("reading error in main\n");
                    exit(0);
                }
            
                //parse the buffer and extract the file path and shift number
                ret = extract(buff,path,shift_num);
                if(ret == -1)
                {
                    perror("parsing error in main\n");
                    exit(0);
                }

                //translate it to int
                shift_num_int = atoi(shift_num);

                //do the rest of the work
                http_server_response(conn_tcp_fd, path, shift_num_int);

                exit(0);    
            }

            //server will close the connection with the client
            if( (ret = close(conn_tcp_fd)) < 0)
            {
                perror("Close connection file descriptor failed\n");
                exit(0);
            }
        }

        //if udp connection is set
        if(FD_ISSET(listen_udp_fd,&rfds))
        {
            client_udp_len = sizeof(client_udp_addr);
            
            //zero out buffers first
            bzero((char *)buff, MAXBUFF);
            bzero((char *)buff_ip,sizeof(buff_ip));
            
            //get IP address and ACK
            if( (ret = recvfrom(listen_udp_fd, (char *) buff, MAXBUFF, 0, 
                                (struct sockaddr *) &client_udp_addr, &client_udp_len)) < 0)
            {
                perror("Error when reading in UDP\n");
                exit(0);
            }

            // because 32 bits = 4 bytes, so ip_len will be ret - 4bytes
            ip_len = ret - 4;

            // copy all the characters inside the ip to buff_ip and buff_ACK
            for(i = 0; i < ip_len; i++) buff_ip[i] = buff[i];
            for(j = 0; j < 4; j++)      buff_ACK[j]= buff[i++];
            
            
            //get ACK
            ACK_int = process_ACK(buff_ACK);
        
            //resolve hostname
            hostname = resolve_name(buff_ip);

            if(hostname == NULL)
            {
                perror("Error while resolving hostname\n");
                exit(0);
            }

            //concat ACK and IP into one string
            out_client = concat_ACK_IP(hostname,ACK_int);

            //get the total length to send
            total_length = strlen(hostname) + 4;

            //send requests 
            if( (ret = sendto(listen_udp_fd, out_client, total_length, 0 ,
                               (struct sockaddr *) &client_udp_addr, client_udp_len) ) < 0 )
            {
                perror("Error when sending in UDP\n");
                exit(0);
            }

            free(out_client);
        }
        
    }

}
